{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Relativistic kinematics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "A basic understanding of relativistic kinematics -- the description of relativistic particle motion --  is required in order to make any quantitative progress in HEP research.\n",
    " \n",
    "The three fundamental principles presented here (without derivation) follow from the requirement that *all particle kinematics must be consistent with the fact that the speed of light is a constant, c, no matter how/where the speed is measured.*\n",
    "\n",
    "> We will show the relevant equations both with factors of *c* and in natural units, so they are familiar both ways. \n",
    "\n",
    "You will need three concepts:\n",
    "1. The relativistic (correct!) definitions of momentum and energy. \n",
    "2. The concept of kinematic 4-vectors.\n",
    "3. The relativistic transformation of kinematic quantities between inertial reference frames (Lorentz transformations)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "##  The relativistic definitions of momentum and energy\n",
    "Consider the motion of a point particle with mass *m* in an inertial reference frame.\n",
    "\n",
    "We can measure\n",
    "* the *position* of the particle in our reference frame, **r** \n",
    "    - this is a \"three-vector\" of three coordinates, *(x,y,z)*\n",
    "* the time in our reference frame, *t*\n",
    "    - a clock carried with the mass would measure a (typically) different time, &tau;.\n",
    "\n",
    "we can then calculate the velocity, $\\mathbf{v} = \\frac{d\\mathbf{r}}{dt}$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In our reference frame, we can define the two useful quantities related to the particle :\n",
    "$$\\vec{\\beta} = \\frac{\\mathbf{v}}{c}$$ \n",
    "and $$\\gamma = 1/\\sqrt{ 1-\\beta^2 }$$\n",
    "\n",
    "Note that in natural units, &beta; is equivalent to v.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "These allow us to define relativistic momentum and energy, and to calculate \"proper time\" (the time measured on a clock carried by the particle).\n",
    "\n",
    "### momentum\n",
    "$$\\mathbf{p} = \\beta \\vec{\\gamma} m \\text{ or } \\mathbf{p} = \\vec{\\beta} \\gamma m c$$\n",
    "\n",
    "### energy (total and kinetic) of a point particle\n",
    "Total energy: $$E =  \\gamma m \\text{ or } E = \\gamma m c^2 $$\n",
    "Kinetic energy: $$E - m  \\text{ or } E - m c^2 $$\n",
    "\n",
    "### proper time\n",
    "$$ \\tau = t/\\gamma$$\n",
    "\n",
    "Did you notice that:\n",
    "* A moving particle has more energy and more time (by a factor of &gamma;)?\n",
    "* For small velocities (&beta; &asymp; 0), momentum is just the non-relativistic *mv*?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "##  Four vectors\n",
    "The location of a particle can be specified by a four component vector\n",
    "$$x^\\mu =(t,x,y,z)=(t,\\mathbf{r}) \\text{ or } (ct,x,y,z)=(ct,\\mathbf{r})$$\n",
    "\n",
    "The superscript &mu; runs from 0 to 3 (how pythonic!).  This way, the spatial coordinate indices  remain 1,2,3.\n",
    "\n",
    "> The subscript &mu; tells you that a quantity is a four-vector: no line or bold font is needed.\n",
    ">> Don't get the superscript confused with an exponent!\n",
    "\n",
    "Other four-vectors:\n",
    "- velocity $u^\\mu = \\frac{dx^\\mu}{d\\tau}$\n",
    "    - What is the four-velocity of a particle at rest?\n",
    "- (energy-)momentum $p^\\mu = m u^\\mu$\n",
    "- force $f^\\mu = \\frac{d p^\\mu}{d\\tau}$\n",
    "\n",
    "> By far, the most commonly used four-vector in high energy physics is the energy-momentum four vector.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Explicitly, the most useful four-vectors are:\n",
    "\n",
    "| spacetime displacement | relativistic velocity | relativistic energy-momentum |\n",
    "|---------------|-----------|----------|\n",
    "|$dx^\\mu = (dt,\\mathbf{dx})$ | $u^\\mu = \\frac{dx^\\mu}{d\\tau}$| $p^\\mu = m \\,u^\\mu$ | \n",
    "|  $(\\gamma d\\tau,\\mathbf{dx})  $  |$(\\gamma,\\gamma \\vec{\\beta})$ | $mc(\\gamma,\\gamma \\vec{\\beta})$ |\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Relativistic Transformations and invariants "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### The dot product in space-time\n",
    "\n",
    "> We're back to natural units from here on!\n",
    "\n",
    "We know that time is special, so it is strange to treat it as just another a component of a \"position\" vector.  Is time orthogonal to space?  Is it just another dimension?\n",
    "\n",
    "\n",
    "\n",
    "What makes relativity work is the definition of the inner (dot) product of four vectors.\n",
    "This is called a \"metric.\"  Defining a metric lets you work in non-orthogonal coordinate systems in general (very useful if you study crystals), and in non-Euclidean coordinate systems like the coordinate system of spacetime."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "A metric looks like a symmetric N &times; N matrix. It has two indices, because it is a scalar function of two vectors.  \n",
    "\n",
    "In terms of this matrix, a generalized dot product can be expressed:\n",
    "\n",
    "$$u \\cdot v = u \\cdot \\hat{G} \\cdot v = \\sum_{\\mu=1}^N \\sum_{\\nu=1}^N u^\\mu g_{\\mu\\nu} v^\\nu$$\n",
    "\n",
    "> The Euclidean metric is diagonal: it's just an identity matrix.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The *Minkowski* metric of spacetime in special relativity looks like\n",
    "\n",
    "\\begin{equation*} \\hat{G} \\equiv g_{\\mu\\nu} = \n",
    "\\begin{pmatrix}\n",
    " 1 & 0 & 0 & 0\\\\\n",
    " 0 & -1 & 0 & 0\\\\\n",
    " 0 & 0 & -1 & 0\\\\\n",
    " 0 & 0 & 0 & -1\\\\\n",
    "\\end{pmatrix}\n",
    "\\end{equation*}\n",
    "\n",
    "Whenever a metric is different from the identity, it creates a new version of any vector $v$, $\\hat{G}\\cdot v$.  These two vectors both carry the same information (because the metric never changes, so it carries no new information.)\n",
    "\n",
    "> ...ok, actually, the metric does change in *general* relativity.  But let's ignore that for now.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's look at the components of the vector multiplied by the metric.\n",
    "\n",
    "$$\\left(\\hat{G} \\cdot x\\right)_\\mu  = \\sum_{\\nu=0}^3 g_{\\mu\\nu} x^{\\nu} \\equiv x_\\mu $$\n",
    "\n",
    "This equation needs some unpacking. Let's use it by substituting $\\mu = 1$.\n",
    "\n",
    "> Remember that the superscripts are indices, not exponents!\n",
    "\n",
    "\\begin{align*}\n",
    "\\left(\\hat{G} \\cdot x\\right)_1  &= \\sum_{\\nu=0}^3 g_{1\\nu} x^{\\nu} \\\\\n",
    "                                &= 0(x^0) + (-1)(x^1) + 0(x^2) + 0(x^3) \\\\\n",
    "x_1 &= -x^1 \n",
    "\\end{align*}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The result might look strange to you.  It comes from writing $[G \\cdot x]_\\mu \\equiv x_\\mu$. How can we reuse the variable name $x$?\n",
    "\n",
    "Remember the comment above: \n",
    "> Whenever a metric is different from the identity, it creates a new version of any vector *v* : *v* and *gv*.  These both carry the same information (because the metric never changes, so it carries no new information.)\n",
    "\n",
    "Since there is no new information, we use the same symbol.  In special relativity, we use the notation $v^\\mu$ for vectors and $v_\\mu$ for vectors-mutiplied-by the metric.  Thus\n",
    "\n",
    "$$v_0=v^0, \\text{ but } v_1=-v^1$$\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Then the dot product of two vectors can be written many ways:\n",
    "\n",
    "$$ u \\cdot v = u \\cdot \\hat{G} \\cdot v = \\sum_{\\mu\\nu} u^\\mu g_{\\mu\\nu} v^\\nu = \\sum_\\mu u^\\mu v_\\mu = \\sum_\\mu u_\\mu v^\\mu$$\n",
    "\n",
    "(the last equality follows from the symmetry of the metric).\n",
    "All of these mean (in Minkowski spacetime):\n",
    "\n",
    "$$ u \\cdot v = u^0 v^0 - u^1 v^1 - u^2 v^2 - u^3 v^3 $$ \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## The invariant mass\n",
    "\n",
    " In Euclidean space, the dot product doesn't change under coordinate rotations.  Thus angles between vectors are invariant, and the *norm* of a vector $\\sqrt{v\\cdot v}$ does not depend on the coordinate system orientation.\n",
    "\n",
    "In special relativity, the dot product is invariant under spatial coordinate rotations (because the spatial part of the *metric* is still proportional to the identity), or under transformations of reference frame called **boosts.**  Two reference frames that (instantaneously) share the same origin, but that have different velocities, are called boosted relative to one another.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "This implies that the velocity should not appear in $\\sqrt{p \\cdot p} = |p|$.  \n",
    "Let's check by evaluating $ |p|^2 = p^0 p_0 + p^1 p_1 + p^2 p_2 + p^3 p_3 = (p^0)^2 - (p^1)^2  - (p^2)^2  - (p^3)^2.$  \n",
    "\n",
    "Recall that $p = m(\\gamma,\\gamma \\vec{\\beta})$...in natural units.\n",
    "\n",
    "\\begin{align*} \n",
    "\\sum_\\mu p^\\mu p_\\mu = \n",
    "            &=  m^2( \\gamma^2 - (\\gamma \\beta^1)^2 -(\\gamma \\beta^2)^2 -(\\gamma \\beta^3)^2 ) \\\\\n",
    "            &= (m)^2 \\left( \\gamma^2 - \\gamma^2 |\\beta|^2 \\right) \\\\   \n",
    "            &= (\\gamma m)^2 \\left( 1 -|\\beta|^2 \\right) \\\\   \n",
    "            &= (\\gamma m)^2 \\frac{1}{\\gamma^2} = m^2 \n",
    "\\end{align*}\n",
    "\n",
    "Indeed, all velocity-dependent terms cancel out! This is the point of the Minkowski metric."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Lorentz transformations\n",
    "\n",
    "### Rotations\n",
    "Rotations in Minkowski spacetime are familiar: What does this do?\n",
    "    \n",
    "\\begin{equation*} \\hat{\\Lambda} \\equiv \\Lambda_\\mu^\\nu = \n",
    "\\begin{pmatrix}\n",
    " 1 & 0 & 0 & 0\\\\\n",
    " 0 & 1 & 0 & 0\\\\\n",
    " 0 & 0 & \\cos \\theta & \\sin \\theta\\\\\n",
    " 0 & 0 & -\\sin \\theta & \\cos \\theta\\\\\n",
    "\\end{pmatrix}\n",
    "\\end{equation*}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Boosts\n",
    "\n",
    "We can construct a transformation corresponding to a boost, because a boost into a frame with velocity $\\beta$ must \n",
    "1. change a particle at rest to one with velocity $-\\beta$ and the same mass,\n",
    "1. change a particle with velocity $\\beta$ into one at rest with the same mass.\n",
    "\n",
    "\n",
    "| Unboosted reference frame     | Boosted reference frame |\n",
    "|----------------|---------|\n",
    "| $(m,\\vec{0})$ |  $\\gamma m, -\\gamma \\vec{\\beta})$ |\n",
    "| $(\\gamma m, \\gamma \\vec{\\beta})$ | $(m,\\vec{0})$  |\n",
    "\n",
    "\n",
    "If the boost is in the x-direction, some algebra shows that this corresponds to  matrix\n",
    "\\begin{equation*} \\hat{\\Lambda} \\equiv \\Lambda_\\mu^\\nu = \n",
    "\\begin{pmatrix}\n",
    " \\gamma & -\\gamma \\beta & 0 & 0\\\\\n",
    " -\\gamma \\beta & \\gamma & 0 & 0\\\\\n",
    " 0 & 0 & 1& 0\\\\\n",
    " 0 & 0 & 0& 1\\\\\n",
    "\\end{pmatrix}\n",
    "\\end{equation*}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Using four-vectors in python\n",
    "\n",
    "Different python packages (ROOT, scikit-hep) provide four-vectors.  Let's practice installing a new package,\n",
    "and importing a class from it.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Requirement already satisfied: scikit-hep in /opt/conda/lib/python3.7/site-packages (0.5.1)\n",
      "Requirement already satisfied: matplotlib>2.0.0; python_version >= \"2.7\" in /opt/conda/lib/python3.7/site-packages (from scikit-hep) (3.1.1)\n",
      "Requirement already satisfied: pandas; python_version >= \"2.7\" in /opt/conda/lib/python3.7/site-packages (from scikit-hep) (0.23.4)\n",
      "Requirement already satisfied: numpy>=1.11.0; python_version >= \"2.7\" in /opt/conda/lib/python3.7/site-packages (from scikit-hep) (1.17.4)\n",
      "Requirement already satisfied: hepunits in /opt/conda/lib/python3.7/site-packages (from scikit-hep) (1.1.0)\n",
      "Requirement already satisfied: kiwisolver>=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib>2.0.0; python_version >= \"2.7\"->scikit-hep) (1.1.0)\n",
      "Requirement already satisfied: python-dateutil>=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib>2.0.0; python_version >= \"2.7\"->scikit-hep) (2.8.1)\n",
      "Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,>=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib>2.0.0; python_version >= \"2.7\"->scikit-hep) (2.4.5)\n",
      "Requirement already satisfied: cycler>=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib>2.0.0; python_version >= \"2.7\"->scikit-hep) (0.10.0)\n",
      "Requirement already satisfied: pytz>=2011k in /opt/conda/lib/python3.7/site-packages (from pandas; python_version >= \"2.7\"->scikit-hep) (2019.3)\n",
      "Requirement already satisfied: setuptools in /opt/conda/lib/python3.7/site-packages (from kiwisolver>=1.0.1->matplotlib>2.0.0; python_version >= \"2.7\"->scikit-hep) (41.6.0.post20191101)\n",
      "Requirement already satisfied: six>=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil>=2.1->matplotlib>2.0.0; python_version >= \"2.7\"->scikit-hep) (1.13.0)\n"
     ]
    }
   ],
   "source": [
    "\n",
    "!python3 -m pip install scikit-hep\n",
    "from skhep.math.vectors import LorentzVector as lv\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(0.0, 0.0, 0.0, 0.0)\n",
      "(0, 0, 250, 500)\n"
     ]
    }
   ],
   "source": [
    "p4 = lv()#make a four-vector (0,0,0,0)\n",
    "print(p4)\n",
    "p4.setpxpypze(0,0,250,500)  #the four components of a 500 GeV particle, moving in the z direction.\n",
    "print(p4)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "mass is: 433.0127018922193\n",
      "energy is: 500\n"
     ]
    }
   ],
   "source": [
    "#Now check your result.\n",
    "print(\"mass is:\",p4.m)\n",
    "print(\"energy is:\",p4.e)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(0.0, 0.0, 0.5)\n",
      "(0.0, 0.0, 0.0, 433.01270189221935)\n"
     ]
    }
   ],
   "source": [
    "p4.setpxpypze(0,0,250,500) \n",
    "print(p4.boostvector)\n",
    "p4 = p4.boost(p4.boostvector)\n",
    "print(p4)\n",
    "#p4.Print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## HW 101.01.004\n",
    "\n",
    "The momentum of a charged particle is measured in natural units to be: $\\vec{p} = ( 0.50, 0.25, -0.30)$ GeV. What is the kinetic energy if it is \n",
    "   1. a pion and \n",
    "   2. a proton?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## HW 101.01.005\n",
    "\n",
    "An unstable neutral particle X decays to a proton and a pion with momenta\n",
    "$\\vec{p}_\\text{proton} = ( 0.35, 0.52, 0.13)$ GeV and $\\vec{p}_\\pi = (-0.23, 0.18, 0.21)$ GeV\n",
    "  1. What is the mass of the X particle?\n",
    "  2.  What is the proton’s momentum in the rest frame of the X particle?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extra: Einstein summation notation and einsum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Einstein summation notation\n",
    "\n",
    "Notice that in the expressions above, the indices that are summed over always appear twice: once as a superscript, and once as a subscript.\n",
    "    \n",
    "Repeated indices are usually summed over in expressions involving matrix multiplication.\n",
    "\n",
    "The \"Einstein summation convention\" exploits this: if an index is repeated, it is summed over: the summation symbol is **assumed.**\n",
    "    \n",
    "> The relativistic version of the convention is to sum over repeated upper and lower indices only.  \n",
    "\n",
    "> numpy's \"einsum\" function sums over *all* repeated indices.  It is really useful for keeping track of axes in very high-dimensional arrays.  (yay physicists!)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "#examples of numpy.einsum\n",
    "import numpy as np\n",
    "m = np.array([1,-1,-1,-1])   #diagonal entries of minkowski metric\n",
    "g = np.zeros((4,4))          #this will be the 4x4 metric; it is all zeroes now.\n",
    "np.fill_diagonal(g,m)        #assign m to the diagonal\n",
    "print(g,\"is the Minkowski metric.\")\n",
    "\n",
    "\n",
    "v = np.array([1,2,3,4]) #example four-vector\n",
    "\n",
    "# Note that \"n\" is repeated, so the second index of the matrix g will be summed over with the only index of the vector v.\n",
    "print(\"\\nThe original four-vector is\",v,\"and the lowered four-vector is\", np.einsum(\"mn,n\",g,v))\n",
    "# In general it is good to keep matrix indices in alphabetical order (here it doesn't matter)\n",
    "print(\"\\nThe actual indices don't matter:\", np.einsum(\"ij,j\",g,v))"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
