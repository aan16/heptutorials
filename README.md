# HEPTutorials

These tutorials can be used in parallel with HEP 101.

## What's here?

Here you will find useful tutorials for getting started with research at the LHC.


## Using this repository with your jupyter notebook

You can download assignments directly into your VM session and modify them by executing the following in a BASH notebook or terminal.
> Note that the directories here are based on Duke OIT vm filesystems.  You can also use google CoLab, cern SWAN, your own laptop, or any other resource that provides a jupyter notebook.

```
cd ~/work
git clone https://gitlab.oit.duke.edu/ath11/heptutorials
```

or in a PYTHON 3 notebook cell, just write `! git clone https://gitlab.oit.duke.edu/ath11/heptutorials `

*You will only need to do this once.*  To get updated or new assignments, in BASH:

```
cd ~/work/heptutorials
git pull
```

or in a PYTHON 3 notebook, just execute `! cd ~/work/heptutorials && git pull`

*Git experts: don't try to push changes.  If you have suggestions for changes to the tutorials, let me know!*

## HEP101 Schedule and homework notebook availability


|  Format    | Topic                                   |  | Notebook available?
| -------- |-----------------------------------------|-------------|-----|
| .mov         | Overview: CERN and research at the LHC  |         | [Yes](https://gitlab.oit.duke.edu/ath11/heptutorials/-/blob/master/NaturalUnits.ipynb) |
| .mov | Relativity and particle kinematics      |         | [Yes](https://gitlab.oit.duke.edu/ath11/heptutorials/-/blob/master/RelativitySKHEP.ipynb) |
|   | Elementary particles/fundamental forces |         | [Yes](https://gitlab.oit.duke.edu/ath11/heptutorials/-/blob/master/ParticlesForces.ipynb), also get `events.hepmc` |
|   | The Standard Model                      |       | n/a |
|   | The ATLAS experiment                    |        | No |
|   | Particle detector principles            |      | n/a |
|  .ppt | [BSM Physics](https://sakai.duke.edu/access/content/group/8c94bbdd-4a95-4ff3-8aec-41ef6fdd0e06/Lectures%202020/HEP101_2020_Al.ppt)             |       |  |
| .pdf  | [Quarks and Gluons](https://sakai.duke.edu/access/content/group/8c94bbdd-4a95-4ff3-8aec-41ef6fdd0e06/Lectures%202020/HEP101-07.pdf)                       |         |  |
| pdf | [Electroweak model and Higgs](http://webhome.phy.duke.edu/~goshaw/HEP101_2017/HEP101_2017_L9.pdf)             |       |  |
| .mp4   | [Heavy Ion Collisions](https://sakai.duke.edu/access/content/group/8c94bbdd-4a95-4ff3-8aec-41ef6fdd0e06/Lectures%202020/HEP101-Heavy-Ion_Collisions_web.mp4)                   |         |  |
|  | Top Physics                             |        |  |
|   | [Physics beyond the Standard Model](http://webhome.phy.duke.edu/~goshaw/HEP101_2017/HEP101_2017_L11.pdf)       |       |  |
